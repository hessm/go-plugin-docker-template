FROM golang AS pluginBuild
ARG pluginRepoUrl

WORKDIR /app

RUN git clone $pluginRepoUrl .
RUN go mod download

RUN CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -buildmode=plugin -o /plugin

FROM golang AS coreBuild

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . ./

RUN CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -o /microservice

FROM golang

COPY --from=pluginBuild /plugin /etc/plugins
COPY --from=coreBuild /microservice /

EXPOSE 3000

CMD ["../microservice"]